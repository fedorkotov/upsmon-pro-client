use log::{debug, info, warn, error};
use std::thread::{sleep};
use std::time::{Duration, Instant};
use std::sync::{Arc};

extern crate log4rs;
extern crate tokio;
extern crate influx_db_client;

#[cfg(test)]
extern crate enum_display_derive;
#[cfg(test)]
#[macro_use] 
extern crate assert_matches;
extern crate system_shutdown;
extern crate config;
extern crate hyper;
extern crate powercom_upsmonpro_state_parser;

mod configuration;
mod influxdb_writer;
mod ups_state_analyzer;
mod ups_state_reader;

use system_shutdown::shutdown;
use configuration::{Parameters};
use powercom_upsmonpro_state_parser::{UPSState};
use influxdb_writer::{InfluxDbUPSStateWriter};
use ups_state_analyzer::{UPSStateAnalyzer, AnalyzerCommand};
use ups_state_reader::read_and_parse_state;

const LOGGING_CONFIG_PATH: &str = "log4rs.yaml";
const TIMESTEP_SEC: u64 = 5;

fn configure_influxdb_writer(parameters: &Parameters) -> Option<Arc<InfluxDbUPSStateWriter>> {
    match  &parameters.influxdb_parameters {
        None => None,
        Some(influxdb_parameters) => {
            debug!("Initializing InfluxDB Writter");
            let influxdb_writer =
                InfluxDbUPSStateWriter::new(
                    influxdb_parameters.url.clone(),
                    influxdb_parameters.dbname.as_str(),                                    
                    influxdb_parameters.measurement.as_str(),
                    influxdb_parameters.upsname.as_str()
                );
            
            match &influxdb_parameters.auth_parameters {
                None => Some(Arc::new(influxdb_writer)),
                Some(auth_parameters) => 
                    Some(Arc::new(influxdb_writer
                        .with_auth(
                            auth_parameters.username.as_str(), 
                            auth_parameters.password.as_str())))
            }             
        }
    }    
}

fn take_action_if_necessary(
    state_analyzer: &mut UPSStateAnalyzer,
    ups_state: Option<&UPSState>){

    match state_analyzer.analyze(ups_state, &Instant::now()) {
        AnalyzerCommand::Warn(msg) => warn!("{}", msg),
        AnalyzerCommand::ShutdownNow(msg) => {
            error!("{}. Shutting down now", msg);

            match shutdown() {
                Ok(_) => info!("System shutdown initiated"),
                Err(error) => eprintln!("Failed to shut down the system: {}", error),
            }
        },
        _ => {}
    };
}

fn send_to_influxdb(
    influxdb_optional_writer: &mut Option<Arc<InfluxDbUPSStateWriter>>,
    ups_state: UPSState){

    if let UPSState::Connected(params) = ups_state {
        if let Some(influxdb_writer_arc) = influxdb_optional_writer {  
            let influxdb_writer_arc_clone = influxdb_writer_arc.clone();
                                                    
            tokio::spawn(
                async move {                            
                        debug!("Writing UPS state to influxdb");
                        match influxdb_writer_arc_clone.write(&params).await {
                            Ok(_) => debug!("State saved to influxdb"),
                            Err(err) => error!("{}", err)   
                        }
                    });
        }
    }
}

async fn _main_loop(
    upsmon_url: &str,
    state_analyzer: &mut UPSStateAnalyzer,
    influxdb_optional_writer: &mut Option<Arc<InfluxDbUPSStateWriter>>)
    -> Result<(), Box<dyn std::error::Error  + Send + Sync + 'static>> {
    
    match read_and_parse_state(upsmon_url, TIMESTEP_SEC).await {
        Ok(ups_state) =>{
            info!("UPS state: {}", &ups_state);

            take_action_if_necessary(
                state_analyzer,
                Some(&ups_state));
            
            send_to_influxdb(
                influxdb_optional_writer,
                ups_state);
            
            Ok(())
        }
        Err(error) => {
            take_action_if_necessary(
                state_analyzer,
                None);

            Err(error)
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    log4rs::init_file(
        LOGGING_CONFIG_PATH,
        Default::default())
    .unwrap();
    
    let parameters = Parameters::configure()?;

    info!("Configuration: {}", parameters);
                            
    debug!("Initializing UPS state analyzer ");
    let mut log_analyzer = 
        UPSStateAnalyzer::new(
            parameters.vin_min_threshold,
            parameters.vin_max_threshold,
            parameters.charge_threshold, 
            parameters.shutdown_delay);
            
    let mut influxdb_optional_writer =
        configure_influxdb_writer(&parameters);

    let timestep = Duration::from_secs(TIMESTEP_SEC);

    debug!("Starting main loop");
    loop {
        let iteration_result =
            _main_loop(
                &parameters.upsmon_url, 
                &mut log_analyzer,
                &mut influxdb_optional_writer).await;

        if let Err(e) = iteration_result {
            error!("{}", e);
        }

        sleep(timestep);
    }
}
