
mod error;
pub use error::Error;

use influx_db_client::{Client, Point, Value};
use influx_db_client::reqwest::Url;

use futures::prelude::*;

use powercom_upsmonpro_state_parser::{UPSStateParameters, UPSMode};

const VIN_FIELD_NAME: &str = "v_input";
const VOUT_FIELD_NAME: &str = "v_output";
const FREQUENCY_FIELD_NAME: &str = "frequency";
const TEMPERATURE_FIELD_NAME: &str = "temperature";
const LOAD_PERCENT_FIELD_NAME: &str = "load_percent";
const CHARGE_PERCENT_FIELD_NAME: &str = "charge_percent";
const UPS_MODE_FIELD_NAME: &str = "ups_mode";
const UPS_NAME_TAG_NAME: &str = "ups_name";

pub struct InfluxDbUPSStateWriter{
    client: Client,
    measurement_name: String,
    ups_name: String   
}

trait InfluxDBStrConvertible {
    fn get_influxdb_str(&self) -> String;
}

impl InfluxDBStrConvertible for UPSMode{
    fn get_influxdb_str(&self) -> String {
        match &self {
            UPSMode::Normal => "normal".to_owned(),
            UPSMode::BoostingVoltage => "boost".to_owned(),
            UPSMode::BuckingVoltage => "buck".to_owned(),
            UPSMode::Bypass => "bypass".to_owned(),
            UPSMode::OnLine => "online".to_owned(),
        }         
    }
}

impl InfluxDbUPSStateWriter {
    pub fn new<T>(
            url: Url, 
            dbname: T,
            measurement_name: T,
            ups_name: T) -> InfluxDbUPSStateWriter 
        where
            T: Into<String>,{
        
        let client = Client::new(url, dbname);        

        InfluxDbUPSStateWriter {
            client,
            ups_name : ups_name.into(),
            measurement_name : measurement_name.into()
        }                
    }
    
    pub fn with_auth<S>(mut self, username: S, password: S) -> Self
        where S: Into<String>
    {
        self.client = 
            self
            .client
            .set_authentication(
                username, 
                password);
        self
    }    

    pub fn write(
        &self, 
        state_parameters: &UPSStateParameters) -> impl Future<Output = Result<(), Error>> {        
            
        let mut point_builder = 
            Point::new(&self.measurement_name)
            .add_tag(
                UPS_NAME_TAG_NAME, 
                Value::String(
                    self.ups_name.to_owned()))        
            .add_field(
                VIN_FIELD_NAME, 
                Value::Integer(
                    state_parameters.voltage_in.into()))
            .add_field(
                VOUT_FIELD_NAME, 
                Value::Integer(
                    state_parameters.voltage_out.into()))
            .add_field(
                FREQUENCY_FIELD_NAME, 
                Value::Integer(
                    state_parameters.frequency.into()))
            .add_field(
                CHARGE_PERCENT_FIELD_NAME, 
                Value::Integer(
                    state_parameters.battery_charge_percent.into()))
            .add_field(
                LOAD_PERCENT_FIELD_NAME, 
                Value::Integer(
                    state_parameters.load_percent.into()))
            .add_field(
                UPS_MODE_FIELD_NAME, 
                Value::String(
                    state_parameters.mode.get_influxdb_str()));
              
                    
        if let Some(t) = state_parameters.temperature{
            point_builder =
                point_builder
                .add_field(
                    TEMPERATURE_FIELD_NAME, 
                    Value::Integer(t.into()));                
        }
            
        self
            .client
            .write_point(point_builder, None, None)
            .map_err(|err| Error::CanNotWritePoint{err})
    }
}