use std::fmt;
use std::error::Error as StdError;

use influx_db_client::Error as InfluxDbClientError;

#[derive(Debug)]
pub enum Error {
    CanNotWritePoint{err: InfluxDbClientError}
}

impl fmt::Display for Error{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::CanNotWritePoint{err} => 
                write!(f, "Could not write UPS state to InfluxDb: {}", err)                
        }
    }
}

impl StdError for Error {
    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        match *self {
            Error::CanNotWritePoint{ref err} => Some(err)
        }
    }
}

impl From<InfluxDbClientError> for Error {
    fn from(err: InfluxDbClientError) -> Error {
        Error::CanNotWritePoint{err}
    }
}
