mod error;
pub use error::Error;
use std::str::FromStr;
use std::time::{Duration};

use powercom_upsmonpro_state_parser::{UPSState};

use tokio::time::timeout;
use log::{debug};
use hyper::{Client, body, Uri};

pub async fn read_and_parse_state(
    upsmon_url: &str,
    timestep_sec: u64) -> 
    Result<UPSState, Box<dyn std::error::Error + Send + Sync>> {

    debug!("Sending GET {}", upsmon_url);

    let uri: Uri = upsmon_url.parse()?;

    let response_t = 
        timeout(
            Duration::from_secs(timestep_sec),
            Client::new().get(uri))
        .await?;

    match response_t {
        Err(e) => Err(Box::from(
                    Error::Timeout{
                        timeout_sec: timestep_sec, 
                        url: upsmon_url.to_owned(), 
                        err: e})),
        Ok(response) => {
            let body_bytes = body::to_bytes(response.into_body()).await?;

            let response_body_text = 
                String::from_utf8(body_bytes.to_vec())?;
            
            debug!("UPSMON Pro raw state received:\n{}", response_body_text);        
            Ok(UPSState::from_str(response_body_text.as_str())?)            
        }
    }        
}
