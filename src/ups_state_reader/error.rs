use std::fmt;
use std::error::Error as StdError;

use hyper::Error as HyperError;

#[derive(Debug)]
pub enum Error {
    Timeout{timeout_sec: u64, url: String, err: HyperError }
}

impl fmt::Display for Error{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::Timeout{timeout_sec, url, err} => 
                write!(
                    f, 
                    "Did not receive response from {} in {} seconds: {}", 
                    url,
                    timeout_sec,
                    err),
        }
    }
}

impl StdError for Error {
    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        match *self {
            Error::Timeout{timeout_sec: _, url: _, ref err} => Some(err)     
        }
    }
}
