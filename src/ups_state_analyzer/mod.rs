use powercom_upsmonpro_state_parser::{UPSState, UPSMode, UPSStateParameters};
use std::time::{Instant, Duration};

mod tests;

// if Ok:
//   last_failure = null
//   timestamp_ups_connection_lost = null
//   -> Ok
// elif Failure:
//   timestamp_ups_connection_lost = null
//   if last_failure == null:
//     last_failure = now
//   time_since_last_failure = now - last_failure
//   if time_since_last_failure > deltaT:
//     -> Shutdown
//   else:
//     -> Warn("No power for {}", time_since_last_failure)
// elif None:
//   if timestamp_ups_connection_lost == null:
//     timestamp_ups_connection_lost = now
//   time_since_connection_lost = now - timestamp_ups_connection_lost
//   if last_failure == null:
//      -> Warn("Connection lost for {}", time_since_connection_lost)
//   else:
//     if time_since_last_failure > deltaT:
//       -> Shutdown
//     else:
//       -> Warn("No power for {} and connection lost for {}", time_since_last_failure, time_since_connection_lost)
//      

pub struct UPSStateAnalyzer {   
    /// If input voltage is less than vin_threshold
    /// analyser assumes that external power failed and
    /// UPS is on batteries 
    vin_min_threshold: u16,    
    vin_max_threshold: u16, 
    min_charge_percent: u8,
    shutdown_delay: Duration,
    last_failure_timestamp: Option<Instant>,
    last_connection_loss_timestamp: Option<Instant>,
    last_state_received_timestamp: Option<Instant>
}

#[derive(Debug)]
pub enum AnalyzerCommand {
    Ok,
    Warn(String),
    ShutdownNow(String)
}

impl UPSStateAnalyzer{
    pub fn new(
        vin_min_threshold: u16, 
        vin_max_threshold: u16, 
        min_charge_percent: u8,
        shutdown_delay: Duration) -> UPSStateAnalyzer {
        
        if vin_max_threshold<vin_min_threshold {
            panic!("vin_max_threshold < vin_min_threshold");
        }

        UPSStateAnalyzer{    
            vin_min_threshold,
            vin_max_threshold,        
            shutdown_delay,
            min_charge_percent,
            last_failure_timestamp: None,
            last_connection_loss_timestamp: None,
            last_state_received_timestamp: None
        }
    }

    /// external power failed or UPS has experienced
    /// some internal malfunction and is in bypass mode
    /// (so is not able to power the system from batteries
    /// in case of external power failure)
    fn _power_failed(&self, ups_state: &UPSStateParameters) -> bool {
        match ups_state.mode {
            UPSMode::Normal  => {
                // UPSMON PRO can report some low (but nonzero)
                // voltage and Normal mode when in fact external power
                // has failed completely and the UPS is on batteries
                ups_state.voltage_in < self.vin_min_threshold ||
                ups_state.voltage_in > self.vin_max_threshold
            },

            UPSMode::BoostingVoltage | 
            UPSMode::BuckingVoltage => false,

            UPSMode::Bypass | 
            UPSMode::OnLine => true
        }
    }

    fn _charge_too_low(&self, ups_state: &UPSStateParameters) -> bool {
        ups_state.battery_charge_percent < self.min_charge_percent
    }

    pub fn get_time_since_power_failed(&self, now: &Instant) -> Option<Duration> {
        match self.last_failure_timestamp {
            None => None,
            Some(t_failure) => Some(now.duration_since(t_failure))
        }
    }

    #[allow(dead_code)]
    pub fn get_time_since_connection_lost(&self, now: &Instant) -> Option<Duration> {
        match self.last_connection_loss_timestamp {
            None => None,
            Some(t_connection_loss) => Some(now.duration_since(t_connection_loss))
        }
    }

    fn _set_failure_time_if_not_set(&mut self, now: &Instant) -> Duration {
        match self.last_failure_timestamp {
            None => {
                self.last_failure_timestamp = Some(now.to_owned());
                Duration::from_secs(0)
            }
            Some(t_last_failure) => {
                now.duration_since(t_last_failure)
            }
        }
    }

    fn _set_connection_loss_time_if_not_set(&mut self, now: &Instant) -> Duration {
        match self.last_connection_loss_timestamp {
            None => {
                // Assuming the worst: we lost connection to UPS
                // the same instant we received last state.

                // No previous state and empty self.last_connection_loss_timestamp
                // means this is the first iteration of main loop.

                let t_connection_loss =
                    match self.last_state_received_timestamp {
                        None => now.to_owned(),
                        Some(timestamp) => timestamp
                    };
                
                self.last_connection_loss_timestamp = Some(t_connection_loss);

                now.duration_since(t_connection_loss)
            }
            Some(t_connection_loss) => {
                now.duration_since(t_connection_loss)
            }
        }
    }

    fn _analyze_connection_loss(&mut self, now: &Instant) -> AnalyzerCommand {
        let time_since_connection_lost =
            self._set_connection_loss_time_if_not_set(&now);

        match self.get_time_since_power_failed(&now) {
            None => {
                AnalyzerCommand::Warn(format!(
                    "No connection to UPS for {} s", 
                    time_since_connection_lost.as_secs()))
            }
            Some(time_since_power_failed) =>{
                let message =
                    format!(
                        "No external power for {} s, connection to UPS lost for {} s", 
                        time_since_power_failed.as_secs(),
                        time_since_connection_lost.as_secs());

                if time_since_power_failed>self.shutdown_delay { 
                    AnalyzerCommand::ShutdownNow(message)
                }
                else {
                    AnalyzerCommand::Warn(message)
                }
            }
        }             
    }

    fn _analyze_ups_state(
        &mut self, 
        state_parameters: &UPSStateParameters, 
        now: &Instant) -> AnalyzerCommand {

        self.last_state_received_timestamp = Some(now.to_owned());
        self.last_connection_loss_timestamp = None;
        if self._charge_too_low(state_parameters) {
            AnalyzerCommand::ShutdownNow(format!(
                "Battery charge level below {} %", 
                self.min_charge_percent))
        }
        else if self._power_failed(state_parameters) {
            let time_since_last_failure = 
                self._set_failure_time_if_not_set(&now);                        
            
            let message = 
                format!(
                    "No external power for {} seconds (V_input = {} V, mode = {})",
                    time_since_last_failure.as_secs(),
                    state_parameters.voltage_in,
                    state_parameters.mode);

            if time_since_last_failure > self.shutdown_delay {
                AnalyzerCommand::ShutdownNow(message)
            }
            else {
                AnalyzerCommand::Warn(message)
            }
        }
        else {
            self.last_failure_timestamp = None;
            AnalyzerCommand::Ok
        }
    }

    /// Returns UPS state description in the form of AnalyzerCommand that can be used
    /// by main program to decide if it's time to shut down the system
    /// 
    /// # Arguments
    /// 
    /// * `ups_state` - None means current UPS state is not available
    /// * `now` - timestamp when ups_state was received
    pub fn analyze(&mut self, ups_state: Option<&UPSState>, now: &Instant) -> AnalyzerCommand {        
        match ups_state {
            Some(ups_state)  => {
                match ups_state {
                    UPSState::Connected(state_parameters) => {
                        self._analyze_ups_state(state_parameters, now)    
                    }

                    // No USB connection UPS - UPSMON PRO
                    UPSState::Disconnected | 
                    UPSState::Disconnecting | 
                    UPSState::Connecting => {
                        self._analyze_connection_loss(now)    
                    }
                }
            }
            // No HTTP Connection UPSMON PRO - upsmon-pro-client
            None => self._analyze_connection_loss(now)
        }
    }
}