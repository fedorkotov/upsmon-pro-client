#[cfg(test)]
use std::time::{Duration, Instant};

#[cfg(test)]
use std::ops::Add;

#[cfg(test)]
use super::{UPSStateAnalyzer, AnalyzerCommand};
#[cfg(test)]
use powercom_upsmonpro_state_parser::{UPSState, UPSMode, MainsState, UPSStateParameters};

// Checking initial state of analyzer
// before first UPS state was received
#[test]
fn initialization() {
    let analyzer = 
        UPSStateAnalyzer::new(
            108,
            240,
            30, 
            Duration::from_secs(30));
    
    let t0 = Instant::now();

    assert_eq!(
        analyzer.get_time_since_connection_lost(&t0),
        None);

    assert_eq!(
        analyzer.get_time_since_power_failed(&t0),
        None);
}

/// The analyzer should return warnings
/// if connection to upsmon pro http server
/// can not be established but should not 
/// shut down the system if
/// power did not fail before connection loss
#[test]
fn  no_http_connection_from_start() {
    let mut analyzer = 
        UPSStateAnalyzer::new( 
            108,
            240,
            30, 
            Duration::from_secs(30));
    
    let t0 = Instant::now();
    let delta_t = Duration::from_secs(5);
    let t1 = t0.add(delta_t);
    let delta_t2 = Duration::from_secs(60);
    let t2 = t0.add(delta_t2);
    
    // timestep 1
    assert_matches!(
        analyzer.analyze(
            None, 
            &t0),
        AnalyzerCommand::Warn(_));
    
    assert_eq!(
        analyzer.get_time_since_connection_lost(&t1),
        Some(delta_t));

    assert_eq!(
        analyzer.get_time_since_power_failed(&t1),
        None);

    // timestep 2
    assert_matches!(
        analyzer.analyze(
            None, 
            &t1),
        AnalyzerCommand::Warn(_));
    
    assert_eq!(
        analyzer.get_time_since_connection_lost(&t2),
        Some(delta_t2));

    assert_eq!(
        analyzer.get_time_since_power_failed(&t2),
        None);

    assert_matches!(
        analyzer.analyze(
            None, 
            &t2),
        AnalyzerCommand::Warn(_));
}

/// The analyzer should return warnings
/// if connection to upsmon pro http server
/// can not be established but should not 
/// shut down the system if
/// power did not fail before connection loss
#[test]
fn  no_usb_connection_from_start() {
    let mut analyzer = 
        UPSStateAnalyzer::new( 
            108,
            240,
            30, 
            Duration::from_secs(30));
    
    let t0 = Instant::now();
    let delta_t = Duration::from_secs(5);
    let t1 = t0.add(delta_t);
    let delta_t2 = Duration::from_secs(60);
    let t2 = t0.add(delta_t2);
    
    // timestep 1
    assert_matches!(
        analyzer.analyze(
            None, 
            &t0),
        AnalyzerCommand::Warn(_));
    
    assert_eq!(
        analyzer.get_time_since_connection_lost(&t1),
        Some(delta_t));

    assert_eq!(
        analyzer.get_time_since_power_failed(&t1),
        None);

    // timestep 2
    assert_matches!(
        analyzer.analyze(
            Some(&UPSState::Disconnecting),
            &t1),
        AnalyzerCommand::Warn(_));
    
    assert_eq!(
        analyzer.get_time_since_connection_lost(&t2),
        Some(delta_t2));

    assert_eq!(
        analyzer.get_time_since_power_failed(&t2),
        None);

    assert_matches!(
        analyzer.analyze(
            None, 
            &t2),
        AnalyzerCommand::Warn(_));
}

/// The analyzer should not
/// detect power failure or produce warnings
/// when external power is good and
/// ups is not on battery power
#[test]
fn  power_good() {
    let mut analyzer = 
        UPSStateAnalyzer::new(
            108,
            240,
            30, 
            Duration::from_secs(30));
    
    let t0 = Instant::now();
    let delta_t = Duration::from_secs(5);
    let t1 = t0.add(delta_t);
    let t2 = t1.add(delta_t);
    

    // timestep 1
    assert_matches!(
        analyzer.analyze(
            Some(&UPSState::Connected(
                UPSStateParameters::new(
                    UPSMode::Normal, 
                    MainsState::Ok, 
                    220, // voltage_in
                    220, // voltage_out
                    15, // load_percent
                    50, // frequency
                    99, // battery_charge_percent
                    Some(30)))), //temperature
            &t0),
        AnalyzerCommand::Ok);
    
    assert_eq!(
        analyzer.get_time_since_connection_lost(&t1),
        None);

    assert_eq!(
        analyzer.get_time_since_power_failed(&t1),
        None);

    // timestep 2
    assert_matches!(
        analyzer.analyze(
            Some(&UPSState::Connected(
                 UPSStateParameters::new(
                    UPSMode::Normal, 
                    MainsState::Ok, 
                    218, // voltage_in
                    218, // voltage_out
                    15, // load_percent
                    50, // frequency
                    98, // battery_charge_percent
                    Some(32)))), //temperature 
            &t1),
        AnalyzerCommand::Ok);
    
    assert_eq!(
        analyzer.get_time_since_connection_lost(&t2),
        None);

    assert_eq!(
        analyzer.get_time_since_power_failed(&t2),
        None);   
}

/// The analyzer should produce warnings
/// on usb connection loss (UPS - upsmon pro) 
/// or http connection loss (upsmon pro - upsmon-pro-client).
/// It should also correctly measure time from connection loss
#[test]
fn  connection_lost() {
    let mut analyzer = 
        UPSStateAnalyzer::new(
            108,
            240,
            30, 
            Duration::from_secs(30));
    
    let t0 = Instant::now();
    let delta_t = Duration::from_secs(5);
    let t1 = t0.add(delta_t);
    let t2 = t1.add(delta_t);
    let t3 = t2.add(delta_t);
    

    // timestep 1 - external power is good
    assert_matches!(
        analyzer.analyze(
            Some(&UPSState::Connected(
                   UPSStateParameters::new(
                    UPSMode::Normal, 
                    MainsState::Ok, 
                    220, // voltage_in
                    220, // voltage_out
                    20, // load_percent
                    50, // frequency
                    100, // battery_charge_percent
                    Some(32)))), //temperature
            &t0),
        AnalyzerCommand::Ok);    

    // timestep 2 - USB connection lost
    assert_matches!(
        analyzer.analyze(
            Some(&UPSState::Disconnected), //temperature 
            &t1),
        AnalyzerCommand::Warn(_));
    
    assert_eq!(
        analyzer.get_time_since_connection_lost(&t2),
        Some(delta_t*2));

    assert_eq!(
        analyzer.get_time_since_power_failed(&t2),
        None); 
        
    // timestep 3 - HTTP connection lost
    assert_matches!(
        analyzer.analyze(
            None, 
            &t2),
        AnalyzerCommand::Warn(_));
    
    assert_eq!(
        analyzer.get_time_since_connection_lost(&t3),
        Some(delta_t*3));

    assert_eq!(
        analyzer.get_time_since_power_failed(&t3),
        None); 
}

/// The analyser should disable warning when
/// usb and http connections are restored
#[test]
fn  connection_restored() {
    let mut analyzer = 
        UPSStateAnalyzer::new(
            108,
            240,
            30, 
            Duration::from_secs(30));
    
    let t0 = Instant::now();
    let delta_t = Duration::from_secs(5);
    let t1 = t0.add(delta_t);
    let t2 = t1.add(delta_t);
    let t3 = t2.add(delta_t);
    
    // timestep 1 - no http connection
    assert_matches!(
        analyzer.analyze(
            None, 
            &t0),
        AnalyzerCommand::Warn(_));    

    // timestep 1 - http connection restored, no USB connection
    assert_matches!(
        analyzer.analyze(
            Some(&UPSState::Connecting), //temperature 
            &t1),
        AnalyzerCommand::Warn(_));        


    // timestep 3 - usb connection restored
    assert_matches!(
        analyzer.analyze(
            Some(&UPSState::Connected(
                UPSStateParameters::new(
                    UPSMode::Normal, 
                    MainsState::Ok, 
                    115, // voltage_in
                    115, // voltage_out
                    60, // load_percent
                    50, // frequency
                    100, // battery_charge_percent
                    Some(25)))), //temperature
            &t2),
        AnalyzerCommand::Ok);
    
    assert_eq!(
        analyzer.get_time_since_connection_lost(&t3),
        None);

    assert_eq!(
        analyzer.get_time_since_power_failed(&t3),
        None);    
}

// If connection is restored and
// upsmon reports power failure
// analyzer should assume that
// power failed at the moment first
// UPS state was received
// (not from the moment when connection was lost)
#[test]
fn  connection_restored_failure() {
    let mut analyzer = 
        UPSStateAnalyzer::new(
            108,
            240,
            30, 
            Duration::from_secs(30));
    
    let t0 = Instant::now();
    let delta_t = Duration::from_secs(5);
    let t1 = t0.add(delta_t);
    let t2 = t1.add(delta_t);
    
    // timestep 1 - no http connection
    assert_matches!(
        analyzer.analyze(
            None, 
            &t0),
        AnalyzerCommand::Warn(_));    

    // timestep 2 - connected, external power failure
    assert_matches!(
        analyzer.analyze(
            Some(&UPSState::Connected(
                    UPSStateParameters::new(
                        UPSMode::OnLine, 
                        MainsState::Failure, 
                        150, // voltage_in
                        220, // voltage_out
                        60, // load_percent
                        50, // frequency
                        85, // battery_charge_percent
                        None))), //temperature
            &t1),
        AnalyzerCommand::Warn(_));
    
    assert_eq!(
        analyzer.get_time_since_connection_lost(&t2),
        None);

    assert_eq!(
        analyzer.get_time_since_power_failed(&t2),
        Some(delta_t));    
}

/// Analyzer should detect mains power failure
/// and shut down the system after specified delay
/// even if charge level is above threshold
#[test]
fn  power_failure_vin_low() {
    let mut analyzer = 
        UPSStateAnalyzer::new(
            108,
            240,
            30, 
            Duration::from_secs(30));
    
    let t0 = Instant::now();
    let delta_t = Duration::from_secs(5);
    let t1 = t0.add(delta_t);
    let t2 = t1.add(delta_t);
    let delta_t2 = Duration::from_secs(35);
    let t3 = t1.add(delta_t2);
    

    // timestep 1 - external power is good
    assert_matches!(
        analyzer.analyze(
            Some(&UPSState::Connected(
                    UPSStateParameters::new(
                        UPSMode::Normal, 
                        MainsState::Ok, 
                        221, // voltage_in
                        221, // voltage_out
                        5, // load_percent
                        50, // frequency
                        100, // battery_charge_percent
                        Some(35)))), //temperature
            &t0),
        AnalyzerCommand::Ok);    

    // timestep 2 - external power fails
    assert_matches!(
        analyzer.analyze(
            Some(&UPSState::Connected(
                    UPSStateParameters::new(
                        UPSMode::OnLine, 
                        MainsState::Failure, 
                        300, // voltage_in
                        220, // voltage_out
                        60, // load_percent
                        50, // frequency
                        85, // battery_charge_percent
                        None))), //temperature
            &t1),
        AnalyzerCommand::Warn(_));
    
    assert_eq!(
        analyzer.get_time_since_connection_lost(&t2),
        None);

    assert_eq!(
        analyzer.get_time_since_power_failed(&t2),
        Some(delta_t)); 
        
    // timestep 3 - power not restored after timeout
    // but battery level is above threshold
    assert_matches!(
        analyzer.analyze(
            Some(
                &UPSState::Connected(
                    UPSStateParameters::new(
                        UPSMode::OnLine, 
                        MainsState::Failure, 
                        0, // voltage_in
                        220, // voltage_out
                        63, // load_percent
                        50, // frequency
                        35, // battery_charge_percent
                        None))), //temperature
            &t3),
        AnalyzerCommand::ShutdownNow(_));
    
    assert_eq!(
        analyzer.get_time_since_connection_lost(&t3),
        None);

    assert_eq!(
        analyzer.get_time_since_power_failed(&t3),
        Some(delta_t2)); 

}

/// If connection to UPS is lost after
/// external power failure the program should assume
/// power is not restored until positive confirmation
/// from UPSMON is received
#[test]
fn  connection_loss_after_power_failure() {
    let mut analyzer = 
        UPSStateAnalyzer::new(
            108,
            240,
            30, 
            Duration::from_secs(30));
    
    let t0 = Instant::now();
    let delta_t = Duration::from_secs(5);
    let t1 = t0.add(delta_t);
    let t2 = t1.add(delta_t);
    let delta_t2 = Duration::from_secs(35);
    let t3 = t1.add(delta_t2);
    

    // timestep 1 - external power is good
    assert_matches!(
        analyzer.analyze(
            Some(
                &UPSState::Connected(
                    UPSStateParameters::new(
                    UPSMode::Normal, 
                    MainsState::Ok, 
                    221, // voltage_in
                    221, // voltage_out
                    5, // load_percent
                    50, // frequency
                    100, // battery_charge_percent
                    Some(35)))), //temperature
            &t0),
        AnalyzerCommand::Ok);    

    // timestep 2 - external power fails
    assert_matches!(
        analyzer.analyze(
            Some(
                &UPSState::Connected(
                    UPSStateParameters::new(
                        UPSMode::OnLine, 
                        MainsState::Failure, 
                        0, // voltage_in
                        220, // voltage_out
                        70, // load_percent
                        50, // frequency
                        90, // battery_charge_percent
                        None))), //temperature
            &t1),
        AnalyzerCommand::Warn(_));
    
    assert_eq!(
        analyzer.get_time_since_connection_lost(&t2),
        None);

    assert_eq!(
        analyzer.get_time_since_power_failed(&t2),
        Some(delta_t)); 
        
    // timestep 3 - http connection lost
    assert_matches!(
        analyzer.analyze(
            None, 
            &t2),
        AnalyzerCommand::Warn(_));
    
    assert_eq!(
        analyzer.get_time_since_connection_lost(&t3),
        Some(delta_t2));

    assert_eq!(
        analyzer.get_time_since_power_failed(&t3),
        Some(delta_t2)); 

    // timestep 4 - http connection not restored before timeout
    assert_matches!(
        analyzer.analyze(
            None, 
            &t3),
        AnalyzerCommand::ShutdownNow(_));    
}

/// The analyzer should detect power failure
/// if input voltage is low even if upsmon pro
/// reports normal mode (which it can do unfortunately)
#[test]
fn power_failure_normal_mode() {
    let mut analyzer = 
        UPSStateAnalyzer::new(
            108,
            240,
            40, 
            Duration::from_secs(30));

    let t0 = Instant::now();
    let t1 = t0.add(Duration::from_secs(5));
    let t2 = t1.add(Duration::from_secs(35));  

    // timestep 1 - external power is good
    assert_matches!(
        analyzer.analyze(
            Some(
                &UPSState::Connected(
                    UPSStateParameters::new(
                    UPSMode::Normal, 
                    MainsState::Ok, 
                    230, // voltage_in
                    230, // voltage_out
                    1,   // load_percent
                    50, // frequency
                    100, // battery_charge_percent
                    Some(25)))), //temperature
            &t0),
        AnalyzerCommand::Ok); 

    // timestep 2 - external power fails.
    // UPS switches to battery power.
    // UPSMON PRO reports low (but non-zero) input voltage
    // but Normal mode and Ok mains state (this happened 
    // when I pulled the plug of UPS out of wall socket)
    assert_matches!(
        analyzer.analyze(
            Some(
                &UPSState::Connected(
                    UPSStateParameters::new(
                    UPSMode::Normal, 
                    MainsState::Ok, 
                    28,  // voltage_in
                    220, // voltage_out
                    1,   // load_percent
                    0,   // frequency
                    100, // battery_charge_percent
                    Some(25)))), //temperature
            &t1),
        AnalyzerCommand::Warn(_)); 

    // timestep 3 - external power was not 
    // restored before timeout
    assert_matches!(
        analyzer.analyze(
            Some(
                &UPSState::Connected(
                    UPSStateParameters::new(
                    UPSMode::Normal, 
                    MainsState::Ok, 
                    25,  // voltage_in
                    220, // voltage_out
                    1,   // load_percent
                    0,   // frequency
                    80, // battery_charge_percent
                    Some(35)))), //temperature
            &t2),
        AnalyzerCommand::ShutdownNow(_)); 
}

/// Analyzer should shut down the system immediately
/// if charge percent falls below threshold
#[test]
fn  chgarge_too_low() {
    let mut analyzer = 
        UPSStateAnalyzer::new(
            108,
            240,
            40, 
            Duration::from_secs(30));
    
    let t0 = Instant::now();
    let delta_t = Duration::from_secs(5);
    let t1 = t0.add(delta_t);
    let t2 = t1.add(delta_t); 

    // timestep 1 - external power is about to fail
    // but UPS is boosting mains voltage and
    // does not switch to battery
    assert_matches!(
        analyzer.analyze(
            Some(
                &UPSState::Connected(
                    UPSStateParameters::new(
                        UPSMode::BoostingVoltage, 
                        MainsState::Warning, 
                        200, // voltage_in
                        230, // voltage_out
                        15, // load_percent
                        50, // frequency
                        95, // battery_charge_percent
                        Some(35)))), //temperature
            &t0),
        AnalyzerCommand::Ok);    

    // timestep 2 - external power is ok, but
    // battery level has fallen below threshold for some reason

    assert_matches!(
        analyzer.analyze(
            Some(
                &UPSState::Connected(
                    UPSStateParameters::new(
                        UPSMode::Normal, 
                        MainsState::Ok, 
                        221, // voltage_in
                        221, // voltage_out
                        5, // load_percent
                        50, // frequency
                        35, // battery_charge_percent
                        Some(35)))), //temperature
            &t1),
        AnalyzerCommand::ShutdownNow(_));
    
    assert_eq!(
        analyzer.get_time_since_connection_lost(&t2),
        None);

    assert_eq!(
        analyzer.get_time_since_power_failed(&t2),
        None);             
}
