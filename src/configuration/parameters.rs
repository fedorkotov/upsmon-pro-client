use std::time::Duration;
use std::fmt;

use influx_db_client::reqwest::Url;

pub struct Parameters {
    pub upsmon_url: String,
    pub vin_min_threshold: u16,
    pub vin_max_threshold: u16,
    pub charge_threshold: u8,
    pub shutdown_delay: Duration,
    pub influxdb_parameters: Option<InfluxDBParameters> 
}

impl Parameters{
    pub fn new(
        upsmon_url: String,
        vin_min_threshold: u16,
        vin_max_threshold: u16,
        charge_threshold: u8,
        shutdown_delay: Duration,
        influxdb_parameters: Option<InfluxDBParameters> 
    ) -> Parameters {
        
        Parameters {
            upsmon_url,
            vin_min_threshold,
            vin_max_threshold,
            charge_threshold,
            shutdown_delay,
            influxdb_parameters
        }
    }
}

impl fmt::Display for Parameters {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f, 
            "{{upsmon_url = '{}'; \
            vin_min_threshold = {} V; \
            vin_max_threshold = {} V; \
            charge_threshold = {} %; \
            shutdown_delay = {} s; \
            influxdb_writter = {}}}",
            self.upsmon_url,
            self.vin_min_threshold,
            self.vin_max_threshold,
            self.charge_threshold,
            self.shutdown_delay.as_secs(),
            match &self.influxdb_parameters {
                None => "Not configured".to_owned(),
                Some(param) => format!("{}", param)
            })
    }
}

pub struct InfluxDBParameters {
    pub url: Url,
    pub dbname: String,
    pub measurement: String,
    pub upsname: String,
    pub auth_parameters: Option<InfluxDBAuthParameters>
}

impl fmt::Display for InfluxDBParameters {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f, 
            "InfluxDB Writter {{url = '{}'; \
             dbname = {}; \
             measurement = {}; \
             upsname = {}; \
             auth = {}}}", 
            self.url, 
            self.dbname, 
            self.measurement, 
            self.upsname,
            match &self.auth_parameters {
                None => "null".to_owned(),
                Some(auth) => format!("\n{}", auth)
            })
    }
}

impl InfluxDBParameters {    
    pub fn new(
        url: Url, 
        dbname: String,
        measurement: String,
        upsname: String,
        auth_parameters: Option<InfluxDBAuthParameters>) -> InfluxDBParameters {
        
        InfluxDBParameters {
            url,
            dbname,
            measurement,
            upsname,
            auth_parameters
        }
    }
}

pub struct InfluxDBAuthParameters {
    pub username: String,
    pub password: String
}

impl fmt::Display for InfluxDBAuthParameters {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f, 
            "{{username: {}; password: ******}}",
            self.username)
    }
}

impl InfluxDBAuthParameters {
    pub fn new(
        username: String,
        password: String) ->  InfluxDBAuthParameters {

        InfluxDBAuthParameters {
            username,
            password
        }
    }
}
