use std::fmt;
use std::num::ParseIntError;
use std::error::Error as StdError;

use config::ConfigError;
use url::ParseError as UrlParseError;


#[derive(Debug)]
pub enum Error {
    HostNameNotSpecified,
    InvalidPortFormat{port_str: String, err: ParseIntError},
    InvalidVoltageFormat{voltage_str: String, err: ParseIntError},
    InvalidChargeFormat{charge: String, err: ParseIntError},
    InvalidDelayFormat{delay_str: String, err: ParseIntError},
    ChargeThresholdTooHigh,
    IncompatibleVoltageLimits,
    InfluxDBMissingURL,
    InfluxDBInvalidURL{url: String, err: UrlParseError},
    InfluxDBNameNotSpecified,
    InfluxDBMeasurementNameNotSpecified,
    InfluxDBUPSNameTagNotSpecified,
    InfluxDBUserNameNotSpecified,
    InfluxDBPasswordNotSpecified,
    ConfigFileParsingError{err: ConfigError}
}

impl fmt::Display for Error{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::HostNameNotSpecified =>
                write!(f, "UPSMON Pro server host name not specified"),
            Error::InvalidPortFormat{port_str, err} =>
                write!(f, "{}' is not a valid port number: '{}'", port_str, err),
            Error::InvalidVoltageFormat{voltage_str, err} =>
                write!(f, "'{}' is not a valid voltage value: '{}'", voltage_str, err),
            Error::InvalidChargeFormat{charge, err} =>
                write!(f, "'{}' is not a valid charge percent value: '{}'", charge, err),
            Error::InvalidDelayFormat{delay_str, err} =>
                write!(f, "'{}' is not a valid delay value: '{}'", delay_str, err),
            Error::ChargeThresholdTooHigh{} =>
                write!(f, "Charge percent threshold can not exceed 100%"),
            Error::IncompatibleVoltageLimits =>
                write!(f, "Lower voltage threshold is equal or above upper voltage threshold"),
            Error::InfluxDBMissingURL =>
                write!(f, "Influxdb url not specified"),
            Error::InfluxDBInvalidURL{url, err} =>
                write!(f, "'{}' is not a valid influxdb url: {}", url, err),
            Error::InfluxDBNameNotSpecified{} =>
                write!(f, "InfluxDb database name not specified"),
            Error::InfluxDBMeasurementNameNotSpecified =>
                write!(f, "InfluxDb measurement name not specified"),
            Error::InfluxDBUPSNameTagNotSpecified =>
                write!(f, "InfluxDb UPS name tag not specified"),
            Error::InfluxDBUserNameNotSpecified =>
                write!(f, "InfluxDb user name not specified"),
            Error::InfluxDBPasswordNotSpecified =>
                write!(f, "InfluxDb password not specified"),
            Error::ConfigFileParsingError{err} =>
                write!(f, "Config file parsing error: {}", err)
        }
    }
}

impl StdError for Error {
    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        match *self {
            Error::HostNameNotSpecified => None,
            Error::InvalidPortFormat{port_str: _, ref err} => Some(err),
            Error::InvalidVoltageFormat{voltage_str: _, ref err} => Some(err),
            Error::InvalidChargeFormat{charge: _, ref err} => Some(err),
            Error::InvalidDelayFormat{delay_str: _, ref err} => Some(err),
            Error::ChargeThresholdTooHigh{} => None,
            Error::IncompatibleVoltageLimits => None,
            Error::InfluxDBMissingURL => None,
            Error::InfluxDBInvalidURL{url: _, ref err} => Some(err),
            Error::InfluxDBNameNotSpecified => None,
            Error::InfluxDBMeasurementNameNotSpecified => None,
            Error::InfluxDBUPSNameTagNotSpecified => None,
            Error::InfluxDBUserNameNotSpecified => None,
            Error::InfluxDBPasswordNotSpecified => None,
            Error::ConfigFileParsingError{ref err} =>  Some(err),
        }
    }
}

impl From<ConfigError> for Error {
    fn from(err: ConfigError) -> Error {
        Error::ConfigFileParsingError{err}
    }
}