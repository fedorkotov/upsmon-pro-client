use log::{trace};

use parameter_validators::*;
use super::*;

use error::Error;

use config::{Config, ConfigError};

const CONFIG_PATH_HOST: &str = "upsmon-pro.host";
const CONFIG_PATH_PORT: &str = "upsmon-pro.port";
const CONFIG_PATH_VIN_MIN: &str = "autoshutdown.vin_min_threshold";
const CONFIG_PATH_VIN_MAX: &str = "autoshutdown.vin_max_threshold";
const CONFIG_PATH_MIN_CHARGE: &str = "autoshutdown.charge_threshold";
const CONFIG_PATH_SHUTDOWN_DELAY: &str = "autoshutdown.delay";

const CONFIG_PATH_INFLUXDB_ENABLED: &str = "influx-db.enabled";
const CONFIG_PATH_INFLUXDB_URL: &str = "influx-db.url";
const CONFIG_PATH_INFLUXDB_DATABASE: &str = "influx-db.database";
const CONFIG_PATH_INFLUXDB_MEASUREMENT: &str = "influx-db.measurement";
const CONFIG_PATH_INFLUXDB_UPSNAME: &str = "influx-db.upsname";
const CONFIG_PATH_INFLUXDB_USERNAME: &str = "influx-db.username";
const CONFIG_PATH_INFLUXDB_PASSWORD: &str = "influx-db.password";

trait OptionalConfigValues {
    fn get_str_or_default(&self, key: &str, default_val: String) -> Result<String, Error>;

    fn get_optional_str(&self, key: &str) -> Result<Option<String>, Error>;

    fn get_bool_or_default(&self, key: &str, default_val: bool) -> Result<bool, Error>;
}

impl OptionalConfigValues for Config {
    fn get_str_or_default(&self, key: &str, default_val: String) -> Result<String, Error>{        
        match self.get_str(key) {
            Ok(string) => Ok(string),
            Err(err) => {
                if let ConfigError::NotFound(_) = err {
                    return Ok(default_val);
                }
                Err(Error::ConfigFileParsingError{err})
            }
        }
    }    

    fn get_optional_str(&self, key: &str) -> Result<Option<String>, Error> {
        match self.get_str(key) {
            Ok(val) => Ok(Some(val)),
            Err(err) => {
                if let ConfigError::NotFound(_) = err {
                    return Ok(None);
                }
                Err(Error::ConfigFileParsingError{err})
            }
        }
    }

    fn get_bool_or_default(&self, key: &str, default_val: bool) -> Result<bool, Error>{        
        match self.get_bool(key) {
            Ok(bool_val) => Ok(bool_val),
            Err(err) => {
                if let ConfigError::NotFound(_) = err {
                    return Ok(default_val);
                }
                Err(Error::ConfigFileParsingError{err})
            }
        }
    } 
}

impl InfluxDBAuthParameters {
    pub fn from_config(settings: &Config) -> Result<Option<Self>, Error> {
        trace!("Reading {}", CONFIG_PATH_INFLUXDB_USERNAME);
        let user_name_option = settings.get_optional_str(CONFIG_PATH_INFLUXDB_USERNAME)?;
        match user_name_option {
            None => Ok(None),
            Some(user_name) =>{
                trace!("Reading {}", CONFIG_PATH_INFLUXDB_PASSWORD);
                let password_option = settings.get_optional_str(CONFIG_PATH_INFLUXDB_PASSWORD)?;

                Ok(Some(InfluxDBAuthParameters::new(
                    get_influxdb_username(user_name)?,
                    get_influxdb_password(password_option)?)))
            }
        }            
    }
}

impl InfluxDBParameters {
    fn _is_enabled_in_config(settings: &Config) -> Result<bool, Error> {
        trace!("Reading {}", CONFIG_PATH_INFLUXDB_ENABLED);
        settings.get_bool_or_default(CONFIG_PATH_INFLUXDB_ENABLED, false)
    }

    pub fn from_config_file(settings: &Config) -> Result<Option<Self>, Error> {                        
        if InfluxDBParameters::_is_enabled_in_config(settings)? {
            trace!("InfluxDB writter is enabled");
            trace!("Reading {}", CONFIG_PATH_INFLUXDB_URL);
            let influxdb_url = 
                get_influxdb_url(
                    settings.get_optional_str(CONFIG_PATH_INFLUXDB_URL)?)?;

            trace!("Reading {}", CONFIG_PATH_INFLUXDB_DATABASE);
            let influxdb_database_name = 
                get_influxdb_dbname(
                    settings.get_optional_str(CONFIG_PATH_INFLUXDB_DATABASE)?)?;

            trace!("Reading {}", CONFIG_PATH_INFLUXDB_MEASUREMENT);
            let influxdb_measurement =
                get_influxdb_measurement(
                    settings.get_optional_str(CONFIG_PATH_INFLUXDB_MEASUREMENT)?)?;

            trace!("Reading {}", CONFIG_PATH_INFLUXDB_UPSNAME);
            let influxdb_upsname =
                get_influxdb_upsname(
                    settings.get_optional_str(CONFIG_PATH_INFLUXDB_UPSNAME)?)?;

            Ok(Some(InfluxDBParameters::new(
                influxdb_url,
                influxdb_database_name,
                influxdb_measurement,
                influxdb_upsname,
                InfluxDBAuthParameters::from_config(settings)?
            )))
        }
        else {
            Ok(None)
        }
    }
}

impl Parameters {
    pub fn from_config_file(config_path: &str) -> Result<Parameters, Error> {                
        trace!("Configuring config file parser");
        let mut settings = config::Config::default();
        settings.merge(
            config::File::with_name(config_path))?;            

        trace!("Reading {}", CONFIG_PATH_HOST);
        let upsmon_pro_host = 
            settings.get_str(CONFIG_PATH_HOST)?;
        
        trace!("Reading {}", CONFIG_PATH_PORT);
        let upsmon_pro_port = 
            get_upsmon_port(
                settings.get_optional_str(CONFIG_PATH_PORT)?)?;       
        
        trace!("Reading {}", CONFIG_PATH_VIN_MIN);
        let vin_min_threshold =
            get_voltage_threshold(
                settings.get_optional_str(CONFIG_PATH_VIN_MIN)?,
                DEFAULT_VIN_MIN_THRESHOLD!())?;

        trace!("Reading {}", CONFIG_PATH_VIN_MAX);
        let vin_max_threshold =
            get_voltage_threshold(
                settings.get_optional_str(CONFIG_PATH_VIN_MAX)?,
                DEFAULT_VIN_MAX_THRESHOLD!())?;

        if vin_min_threshold >= vin_max_threshold {
            return Err(Error::IncompatibleVoltageLimits);
        }

        trace!("Reading {}", CONFIG_PATH_MIN_CHARGE);
        let charge_threshold =
            get_charge_threshold(
                settings.get_optional_str(CONFIG_PATH_MIN_CHARGE)?)?;

        trace!("Reading {}", CONFIG_PATH_SHUTDOWN_DELAY);
        let shutdown_delay =
            get_shutdown_delay(
                settings.get_optional_str(CONFIG_PATH_SHUTDOWN_DELAY)?)?;

        Ok(Parameters{
            upsmon_url: 
                format!(
                    "http://{}:{}/ups.txt",
                    upsmon_pro_host,
                    upsmon_pro_port),
            vin_min_threshold,
            vin_max_threshold,
            charge_threshold,
            shutdown_delay,
            influxdb_parameters: 
                InfluxDBParameters::from_config_file(&settings)?})
    }
}
