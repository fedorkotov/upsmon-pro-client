#![macro_use]

pub const UPSMON_PRO_DEFAULT_PORT: &str = "8000";
pub const UPSMON_PRO_DEFAULT_DBNAME: &str = "telegraf";
pub const UPSMON_PRO_DEFAULT_MEASUREMENT: &str = "ups";

// As of today (2020-02-10) macro_rules and built in
// concat!() is the only way of converting numeric constants
// to strings at compile time

macro_rules! DEFAULT_DELAY { () => (60)}

macro_rules! DEFAULT_CHARGE_THRESHOLD { () => (50)}

macro_rules! DEFAULT_VIN_MIN_THRESHOLD { () => (190)}

macro_rules! DEFAULT_VIN_MAX_THRESHOLD { () => (250)}