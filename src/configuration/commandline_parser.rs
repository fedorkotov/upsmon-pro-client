use clap::{Arg, App, ArgMatches};
use log::{debug};

use constants::*;
use parameter_validators::*;
use error::Error;
use super::*;

pub const ARGNAME_CONFIG_FILE_PATH: &str = "config";
pub const ARGNAME_UPSMON_PRO_PORT: &str = "upsmon-port";
pub const ARGNAME_UPSMON_PRO_HOST: &str = "upsmon-host";
pub const ARGNAME_INFLUXDB_URL: &str = "influxdb-url";
pub const ARGNAME_INFLUXDB_USER: &str = "influxdb-user";
pub const ARGNAME_INFLUXDB_PASSWORD: &str = "influxdb-password";
pub const ARGNAME_INFLUXDB_DBNAME: &str = "influxdb-database";
pub const ARGNAME_INFLUXDB_MEASUREMENT: &str = "influxdb-measurement";
pub const ARGNAME_INFLUXDB_UPSNAME: &str = "upsname";
pub const ARGNAME_CHARGE_THRESHOLD: &str = "charge-threshold";
pub const ARGNAME_SHUTDOWN_DELAY: &str = "shutdown-delay";
pub const ARGNAME_VIN_MAX_THRESHOLD: &str = "vin-max";
pub const ARGNAME_VIN_MIN_THRESHOLD: &str = "vin-min";

const HELP_MSG: &str =
    concat!("Examples:
    
    ",env!("CARGO_PKG_NAME")," --upsmon-host 192.168.0.2
    
    ",env!("CARGO_PKG_NAME")," --upsmon-host 192.168.0.2 --upsmon-port 50012 \\
        --influx-db-url http://127.0.0.1:8086 --influxdb-database telegraf
    
    ",env!("CARGO_PKG_NAME")," -c upsmon-pro-client.toml");

impl InfluxDBAuthParameters {
    pub fn from_commandline(argmatches: &'_ ArgMatches) -> Result<Option<InfluxDBAuthParameters>, Error> {
        match argmatches.value_of(ARGNAME_INFLUXDB_USER) {
            None => Ok(None),
            Some(username) => 
                Ok(Some(InfluxDBAuthParameters::new(
                    get_influxdb_username(username)?,
                    get_influxdb_password(
                        argmatches
                        .value_of(ARGNAME_INFLUXDB_PASSWORD))?)))
        }  
    }
}

impl InfluxDBParameters {
    pub fn from_commandline(argmatches: &'_ ArgMatches) -> Result<Option<InfluxDBParameters>, Error> {
        match  argmatches.value_of(ARGNAME_INFLUXDB_URL) {
            None => Ok(None),
            Some(url) => {                
                Ok(Some(InfluxDBParameters::new(
                    get_influxdb_url(Some(url))?,
                    get_influxdb_dbname(
                        argmatches
                        .value_of(ARGNAME_INFLUXDB_DBNAME))?,
                    get_influxdb_measurement(
                        argmatches
                        .value_of(ARGNAME_INFLUXDB_MEASUREMENT))?,
                    get_influxdb_upsname(
                        argmatches
                        .value_of(ARGNAME_INFLUXDB_UPSNAME))?,
                    InfluxDBAuthParameters::from_commandline(argmatches)?
                )))
            }
        }   
    }
}

impl Parameters {   
    
    pub fn configure() -> Result<Parameters, Error> {
        let argmatches = 
            Parameters::get_arg_parser()
            .get_matches();
        
        if let Some(cofig_path) = argmatches.value_of(ARGNAME_CONFIG_FILE_PATH) {
            debug!("Parameters will be read from config file '{}'", cofig_path);
            return Parameters::from_config_file(cofig_path);
        }
        
        debug!("Parameters will be set from command line arguments");

        let upsmon_host = 
            get_upsmon_host(
                argmatches.value_of(ARGNAME_UPSMON_PRO_HOST))?;
        let upsmon_port = 
            get_upsmon_port(
                argmatches.value_of(ARGNAME_UPSMON_PRO_PORT))?;                
        
        let vin_min_threshold = 
            get_voltage_threshold(
                argmatches.value_of(ARGNAME_VIN_MIN_THRESHOLD),
                DEFAULT_VIN_MIN_THRESHOLD!())?;
        let vin_max_threshold = 
            get_voltage_threshold(
                argmatches.value_of(ARGNAME_VIN_MAX_THRESHOLD), 
                DEFAULT_VIN_MAX_THRESHOLD!())?;

        if vin_min_threshold >= vin_max_threshold {
            return Err(Error::IncompatibleVoltageLimits);
        }

        let charge_threshold =
            get_charge_threshold(
                argmatches.value_of(ARGNAME_CHARGE_THRESHOLD))?;

        let shutdown_delay =
            get_shutdown_delay(
                argmatches.value_of(ARGNAME_SHUTDOWN_DELAY))?;

        Ok(Parameters::new(
            format!("http://{}:{}/ups.txt", upsmon_host, upsmon_port),
            vin_min_threshold,
            vin_max_threshold,
            charge_threshold,
            shutdown_delay,
            InfluxDBParameters::from_commandline(&argmatches)?
        ))
    }
        
    pub fn get_arg_parser<'c,'d>() -> App<'c,'d> {
        App::new("UPSMON PRO client")
            .version(env!("CARGO_PKG_VERSION"))
            .author(env!("CARGO_PKG_AUTHORS"))
            .about("Periodically reads POWERCOM UPS state from http server provided by 
UPSMON PRO utility and optionally sends it to InfluxDB.
Shuts down the system on external power failure with user configurable delay.")
            .arg(Arg::with_name(ARGNAME_CONFIG_FILE_PATH)
                    .short("c")
                    .long(ARGNAME_CONFIG_FILE_PATH)
                    .value_name("CONFIG_PATH")
                    .help("configuration file path")                
                    .takes_value(true)
                    .required(false))        
            .arg(Arg::with_name(ARGNAME_UPSMON_PRO_HOST)
                    .short("h")          
                    .long(ARGNAME_UPSMON_PRO_HOST)
                    .value_name("HOST")
                    .help("UPSMON-PRO host")
                    .required(true)
                    .conflicts_with(ARGNAME_CONFIG_FILE_PATH)
                    .takes_value(true))
            .arg(Arg::with_name(ARGNAME_UPSMON_PRO_PORT)
                    .short("p")
                    .long(ARGNAME_UPSMON_PRO_PORT)
                    .value_name("PORT")
                    .help("UPSMON-PRO port")
                    .takes_value(true)
                    .conflicts_with(ARGNAME_CONFIG_FILE_PATH)
                    .default_value_if(ARGNAME_UPSMON_PRO_HOST, None, UPSMON_PRO_DEFAULT_PORT))  
            .arg(Arg::with_name(ARGNAME_VIN_MIN_THRESHOLD)   
                    .long(ARGNAME_VIN_MIN_THRESHOLD)                
                    .value_name("VOLTAGE")
                    .help("Input voltage lower threshold")
                    .required(false)
                    .conflicts_with(ARGNAME_CONFIG_FILE_PATH)
                    .default_value_if(ARGNAME_UPSMON_PRO_HOST, None, concat!(DEFAULT_VIN_MIN_THRESHOLD!()))
                    .takes_value(true))
            .arg(Arg::with_name(ARGNAME_VIN_MAX_THRESHOLD)   
                    .long(ARGNAME_VIN_MAX_THRESHOLD)
                    .required(false)
                    .conflicts_with(ARGNAME_CONFIG_FILE_PATH)
                    .default_value_if(ARGNAME_UPSMON_PRO_HOST, None, concat!(DEFAULT_VIN_MAX_THRESHOLD!()))
                    .value_name("VOLTAGE")
                    .help("Input voltage upper threshold")
                    .takes_value(true))                      
            .arg(Arg::with_name(ARGNAME_CHARGE_THRESHOLD)   
                    .long(ARGNAME_CHARGE_THRESHOLD)
                    .required(false)
                    .conflicts_with(ARGNAME_CONFIG_FILE_PATH)
                    .default_value_if(ARGNAME_UPSMON_PRO_HOST, None, concat!(DEFAULT_CHARGE_THRESHOLD!()))
                    .value_name("CHARGE_PERCENT")
                    .help("Battery charge threshold (percent)")
                    .takes_value(true))
            .arg(Arg::with_name(ARGNAME_SHUTDOWN_DELAY)   
                    .long(ARGNAME_SHUTDOWN_DELAY)
                    .required(false)
                    .conflicts_with(ARGNAME_CONFIG_FILE_PATH)
                    .default_value_if(ARGNAME_UPSMON_PRO_HOST, None, concat!(DEFAULT_DELAY!()))
                    .value_name("DELAY")
                    .help("Shutdown delay (seconds) after external power failure")
                    .takes_value(true))
            .arg(Arg::with_name(ARGNAME_INFLUXDB_URL)
                    .short("i")          
                    .long(ARGNAME_INFLUXDB_URL)
                    .required(false)
                    .value_name("INFLUX_DB_URL")
                    .help("InfluxDB Url")
                    .conflicts_with(ARGNAME_CONFIG_FILE_PATH)
                    .requires(ARGNAME_INFLUXDB_DBNAME)
                    .requires(ARGNAME_INFLUXDB_UPSNAME)
                    .long_help("If specified, program will log UPS state to InfluxDB")
                    .takes_value(true))
            .arg(Arg::with_name(ARGNAME_INFLUXDB_USER)   
                    .long(ARGNAME_INFLUXDB_USER)
                    .required(false)
                    .value_name("USER")
                    .help("InfluxDB user name")
                    .conflicts_with(ARGNAME_CONFIG_FILE_PATH)
                    .requires(ARGNAME_INFLUXDB_URL)
                    .requires(ARGNAME_INFLUXDB_PASSWORD)
                    .takes_value(true))
            .arg(Arg::with_name(ARGNAME_INFLUXDB_PASSWORD)   
                    .long(ARGNAME_INFLUXDB_PASSWORD)
                    .required(false)
                    .value_name("PASSWORD")
                    .help("InfluxDB password")
                    .conflicts_with(ARGNAME_CONFIG_FILE_PATH)
                    .requires(ARGNAME_INFLUXDB_URL)
                    .requires(ARGNAME_INFLUXDB_USER)
                    .takes_value(true))
            .arg(Arg::with_name(ARGNAME_INFLUXDB_DBNAME)   
                    .long(ARGNAME_INFLUXDB_DBNAME)
                    .required(false)
                    .value_name("DBNAME")
                    .help("InfluxDB database name")
                    .conflicts_with(ARGNAME_CONFIG_FILE_PATH)
                    .requires(ARGNAME_INFLUXDB_URL)
                    .default_value_if(ARGNAME_INFLUXDB_URL, None, UPSMON_PRO_DEFAULT_DBNAME)
                    .takes_value(true))
            .arg(Arg::with_name(ARGNAME_INFLUXDB_MEASUREMENT)   
                    .long(ARGNAME_INFLUXDB_MEASUREMENT)
                    .required(false)
                    .value_name("DBNAME")
                    .help("InfluxDB database name")
                    .requires(ARGNAME_INFLUXDB_URL)
                    .conflicts_with(ARGNAME_CONFIG_FILE_PATH)
                    .default_value_if(ARGNAME_INFLUXDB_URL, None, UPSMON_PRO_DEFAULT_MEASUREMENT)
                    .takes_value(true))
            .arg(Arg::with_name(ARGNAME_INFLUXDB_UPSNAME)   
                    .long(ARGNAME_INFLUXDB_UPSNAME)
                    .required(false)
                    .value_name("UPSNAME")
                    .help("UPS name (used as tag for influxdb measurements)")
                    .conflicts_with(ARGNAME_CONFIG_FILE_PATH)
                    .requires(ARGNAME_INFLUXDB_URL)
                    .takes_value(true))
            .after_help(HELP_MSG)
    }    
}
