use std::time::Duration;
use std::str::FromStr;

use constants::*;
use super::*;
use error::Error;

use influx_db_client::reqwest::Url;

pub fn get_upsmon_host<T: Into<String>>(
    host_str: Option<T>) -> Result<String, Error> {

    match host_str{
        None => Err(Error::HostNameNotSpecified),
        Some(host) => Ok(host.into())
    }        
}

pub fn get_upsmon_port<T: Into<String>>(
    portnumber_opt: Option<T>) -> Result<u32, Error> {    

    let effective_portnumber_str =
        match portnumber_opt {
            None => UPSMON_PRO_DEFAULT_PORT.to_owned(),
            Some(portnumber_str) => portnumber_str.into()
        };
    
    effective_portnumber_str
        .parse::<u32>()
        .map_err(
            |err| Error::InvalidPortFormat {
                    port_str: effective_portnumber_str,
                    err})
}

pub fn get_voltage_threshold<T: Into<String>>(
    voltage_opt_str: Option<T>, 
    default_val: u16) -> Result<u16, Error> {    

    match voltage_opt_str {
        None => Ok(default_val),
        Some(voltage_str) => {
            let voltage_string = voltage_str.into();

            voltage_string
            .parse::<u16>()
            .map_err(
                |err| Error::InvalidVoltageFormat {
                        voltage_str: voltage_string,
                        err})
        }
    }        
}

pub fn get_charge_threshold<T: Into<String>>(
    charge_opt_str: Option<T>) -> Result<u8, Error> {            

    match charge_opt_str {
        None => Ok(DEFAULT_CHARGE_THRESHOLD!()),
        Some(charge_str) => {
            let charge_string = charge_str.into();

            match charge_string.parse::<u8>() {
                Ok(charge) => {
                    if charge > 100 {
                        Err(Error::ChargeThresholdTooHigh)
                    }
                    else {
                        Ok(charge)
                    }
                }
                Err(err) => Err(Error::InvalidChargeFormat {
                    charge: charge_string,
                    err})
            }
        }
    }            
}

pub fn get_shutdown_delay<T: Into<String>>(
    delay_opt_str: Option<T>) -> Result<Duration, Error> {        

    match delay_opt_str {
        None => Ok(Duration::from_secs(DEFAULT_DELAY!())),
        Some(delay_str) => {
            let delay_string = delay_str.into();

            match delay_string.parse::<u64>() {
                Ok(delay) => Ok(Duration::from_secs(delay)),
                Err(err) => Err(Error::InvalidDelayFormat {
                    delay_str: delay_string,
                    err})
            }
        }
    }            
}

pub fn get_influxdb_url<T: Into<String>>(
    url_opt_str: Option<T>) -> Result<Url, Error> {

    match url_opt_str {
        None => Err(Error::InfluxDBMissingURL),
        Some(url_str) => {
            let url_string = url_str.into();

            Url::from_str(&url_string)
            .map_err(
                |err| Error::InfluxDBInvalidURL{ 
                    url: url_string, 
                    err})
        }
    }
}

fn _check_not_empty_or_default<T: Into<String>>(
    opt_str: Option<T>,
    default_if_none: &str ,
    err_if_empty: Error) -> Result<String, Error> {

    match opt_str {
        None => Ok(default_if_none.to_owned()),
        Some(str_val) => {
            let string = str_val.into();
            if string.chars().count() == 0 {
                Err(err_if_empty)
            }
            else{
                Ok(string)
            }            
        }
    }
}

fn _check_not_empty<T: Into<String>>(
    str_val: T,
    err_if_empty: Error) -> Result<String, Error>{

    let string = str_val.into();
    if string.chars().count() == 0 {
        Err(err_if_empty)
    }
    else{
        Ok(string)
    }    
}

fn _check_not_none_not_empty<T: Into<String>>(
    opt_str: Option<T>,
    err_if_empty: Error) -> Result<String, Error> {

    match opt_str {
        None => Err(err_if_empty),
        Some(str_val) => 
            _check_not_empty(
                str_val, 
                err_if_empty)
    }
}

pub fn get_influxdb_dbname<T: Into<String>>(
    dbname_opt_str: Option<T>) -> Result<String, Error> {

    _check_not_empty_or_default(
        dbname_opt_str,
        UPSMON_PRO_DEFAULT_DBNAME,
        Error::InfluxDBNameNotSpecified)
}

pub fn get_influxdb_measurement<T: Into<String>>(
    measurement_opt_str: Option<T>) -> Result<String, Error> {

    _check_not_empty_or_default(
        measurement_opt_str,
        UPSMON_PRO_DEFAULT_MEASUREMENT,
        Error::InfluxDBMeasurementNameNotSpecified)
}

pub fn get_influxdb_upsname<T: Into<String>>(
    upsname_opt_str: Option<T>) -> Result<String, Error> {

    _check_not_none_not_empty(
        upsname_opt_str,
        Error::InfluxDBUPSNameTagNotSpecified)
}

pub fn get_influxdb_username<T: Into<String>>(
    username_opt_str: T) -> Result<String, Error> {

    _check_not_empty(
        username_opt_str,
        Error::InfluxDBUserNameNotSpecified)
}

pub fn get_influxdb_password<T: Into<String>>(
    password_opt_str: Option<T>) -> Result<String, Error> {

    _check_not_none_not_empty(
        password_opt_str,
        Error::InfluxDBPasswordNotSpecified)
}
