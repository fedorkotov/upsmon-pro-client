#!/usr/bin/env bash
DISTR_PATH="./distr"
DISTR_FILES_PATH="./bin"
DISTR_OUT_PATH="./"

cargo build --release

mkdir -p "$DISTR_FILES_PATH"
cp target/release/upsmon-pro-client "$DISTR_FILES_PATH/"
cp "$DISTR_PATH/upsmon-pro-client.toml" "$DISTR_FILES_PATH/"
cp "$DISTR_PATH/log4rs.yaml" "$DISTR_FILES_PATH/"
cp "$DISTR_PATH/setup.sh" "$DISTR_FILES_PATH/"
cp LICENSE "$DISTR_FILES_PATH/"
cp "$DISTR_PATH/upsmon-pro-client.service" "$DISTR_FILES_PATH/"
makeself/makeself.sh \
    --license LICENSE \
    --needroot \
    "$DISTR_FILES_PATH" \
    "$DISTR_OUT_PATH/upsmon-pro-client-installer.sh" \
    "UPSMON PRO Client" \
    ./setup.sh