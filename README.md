[![pipeline][s1]][l1] [![MIT][s2]][l2] 

[s1]: https://gitlab.com/fedorkotov/upsmon-pro-client/badges/master/pipeline.svg
[l1]: https://gitlab.com/fedorkotov/upsmon-pro-client/-/pipelines/latest

[s2]: https://img.shields.io/badge/license-MIT-blue.svg
[l2]: https://gitlab.com/fedorkotov/upsmon-pro-client/-/blob/master/LICENSE

# upsmon-pro-client

[[_TOC_]]

## What is this?

![alt text](doc/integration-scheme.png "Logo Title Text 1")

upsmon-pro-client is a Linux service and/or console utility written in [rust](https://www.rust-lang.org/) with 3 main functions

1. reads [POWERCOM](https://www.upspowercom.com/) UPS state provided by Windows versions of [UPSMON Pro](https://www.upspowercom.com/PRO-Windows.jsp) via HTTP. 
2. shuts down host machine when it detects power failure
3. optionally writes UPS state to [InfluxDB](https://www.influxdata.com/products/influxdb-overview/) time series database

I am not affiliated with POWERCOM, the utility is unofficial and is not endorsed by POWERCOM.

## Why not use pre-existing UPS drivers and monitoring software?

My home server runs Ubuntu Server as host OS but POWERCOM SPD-850 UPS that powers it refused to talk to POWERCOM's own Linux software (UPSMON Pro) or NUT (https://networkupstools.org/). 
So I had to use Windows version of UPSMON Pro in a virtual machine with USB device pass-through (Windows XP with 128 Mb RAM is enough) and a custom Linux service that reads UPS state via HTTP. This was much simpler and more alined with my interests than learning Linux USB programming and patching NUT's Powercom driver to work with my UPS (if it is even possible). RAM is cheap. 128 Mb for VM is not that much. 

I probably could send shutdown command to host machine from UPSMON Pro (it can run arbitrary scripts before system shutdown) but I also wanted to log line voltage and UPS state to InfluxDB and writing my own service was much more fun.

## Why rust?

I like rust and wanted some practice.

## Why InfluxDB?

I use [Telegraf](https://www.influxdata.com/time-series-platform/telegraf/) and [Grafana](https://grafana.com/) with [InfluxDB](https://www.influxdata.com/products/influxdb-overview/) as data store for server monitoring so InfluxDB was a logical choice for me.

## Installation

### Installing as systemd service on x86_64 Linux

Download and run `upsmon-pro-client-installer.sh` installer script with sudo or from root. The script was generated with [makeself](https://makeself.io/) and should work on any x86_64 systemd-based distro but I only tested it on Ubuntu 18.04.

The script requires root privileges. Main executable and [config files](#Configuration) are installed to `/opt/upsmon-pro-client/`. Installer also installs, enables and starts `upsmon-pro-client` systemd service.
Installer does not replace config file if it already exists.

### Installing on non-systemd Linux distros and other OSes

Prebuilt upsmon-pro-client executable can be used as a console utility on any x86_64 Linux distribution as is but you will have to install it as a service by yourself in a way specific to your init system. 
Automatic shutdown functionality provided by [
system_shutdown](https://crates.io/crates/system_shutdown) rust crate supports only Linux, MacOS and Windows. On other systems it may not compile. For anything but x86_64 Linux you will have to [compile](#compilation-instructions) the code yourself.

On Windows you can install [UPSMON Pro](https://www.upspowercom.com/PRO-Windows.jsp) utility distributed by UPS manufacturer so you have no need for upsmon-pro-client. But upsmon-pro-client should compile and work just fine as a standalone console tool.

## Compilation instructions

To compile upsmon-pro-client main executable use the usual cargo command
```sh
cargo build --release
```
After successful compilation executable will be in `target/release/` folder

To build service installer `upsmon-pro-client-installer.sh` like so (assumes that current directory is repository root)
```sh
DISTR_PATH="./distr"
DISTR_FILES_PATH="./bin"
DISTR_OUT_PATH="./"

mkdir -p distr
cp target/release/upsmon-pro-client "$DISTR_FILES_PATH/"
cp "$DISTR_PATH/upsmon-pro-client.toml" "$DISTR_FILES_PATH/"
cp "$DISTR_PATH/log4rs.yaml" "$DISTR_FILES_PATH/"
cp "$DISTR_PATH/setup.sh" "$DISTR_FILES_PATH/"
cp LICENSE "$DISTR_FILES_PATH/"
cp "$DISTR_PATH/upsmon-pro-client.service" "$DISTR_FILES_PATH/"
makeself/makeself.sh \
    --license LICENSE \
    --needroot \
    "$DISTR_FILES_PATH" \
    "$DISTR_OUT_PATH/upsmon-pro-client-installer.sh" \
    "UPSMON PRO Client" \
    ./setup.sh
```

`make-distr.sh` script in repository root folder will perform both of above

## Configuration

The service has two configuration files

* `/opt/upsmon-pro-client/upsmon-pro-client.toml` contains UPSMON Pro connection options, automatic shutdown options and InfluxDB connection options. See also https://en.wikipedia.org/wiki/TOML. All options can also be provided as command-line arguments. This configuration file is not replaced by [installer script](#installing-as-systemd-service-on-x86_64-linux) if it exists. This file is used by systemd service. When upsmon-pro-client is used as standalone command line utility you can specify any other config path you want (see [--config](#setting-config-file-path) command line argument description). `/opt/upsmon-pro-client/upsmon-pro-client.toml.default` is a reference copy of default config for currently installed version. This file is ignored by the service.
* `/opt/upsmon-pro-client/log4rs.yaml` contains logging options

### UPSMON Pro connection configuration

UPSMON Pro connection settings are located in `[upsmon-pro]` section of `upsmon-pro-client.toml` config file.

* `host` string parameter (`--upsmon-host` command line argument) is a name or IP address of machine where [UPSMON Pro](https://www.upspowercom.com/PRO-Windows.jsp) is installed
* `port` integer parameter (`--upsmon-port` command line argument) is port number on which UPSMON Pro HTTP server is listening

### Automatic shutdown configuration

Automatic shutdown settings are located in `[autoshutdown]` section of `upsmon-pro-client.toml` config file. 
1. If battery charge level falls below configured threshold value, system shutdown is initiated immediately (configured delay is ignored) even if UPS is not on battery power at the moment because otherwise UPS may not support the system long enough for orderly shutdown when power fails in next few seconds.
1. Shutdown is initiated if UPSMON PRO reports that UPS is on battery power and external power is not restored before configured delay 
1. Shutdown is initiated if in normal operation mode (not on batteries) input voltage drops below lower or above upper threshold value and is not restored before configured delay. This condition was added to overcome UPSMON PRO misreporting of external power failure for my UPS (see https://gitlab.com/fedorkotov/upsmon-pro-client/-/issues/15).

* `vin_min_threshold`, `vin_max_threshold` integer parameters (`--vin-min`, `--vin-max` command line arguments) are lower and upper input (mains) voltage thresholds in volts. Parameters can be omitted. Default lower threshold is 190 V. Default upper threshold is 250 V.
* `charge_threshold` integer parameter (`--charge-threshold` command line argument) is minimum charge level in percent. The parameter can be omitted. Default value is 50 percent.
* `delay` integer parameter (`--shutdown-delay` command line argument) is delay in seconds before system shutdown after power failure. The parameter can be omitted. Default value is 60 seconds.

### InfluxDB connection configuration

InfluxDB connection settings are located in `[influx-db]` section of `upsmon-pro-client.toml` config file. The section can be omitted if sending UPS state to InfluxDB is not required. 
ups-mon-pro was tested with InfluxDB v1.8. [influx_db_client](https://crates.io/crates/influx_db_client) rust crate that was used as InfluxDB client also supports InfluxDB v1.0.2/v1.3.5/v1.5. It is probably not compatible with v2.0 because InfluxDB write API has changed significantly.

* `enabled` boolean parameter. If enabled is false the rest of `[influx-db]` section is ignored and UPS state is not written to InfluxDB.
* `url` string parameter (`--influxdb-url` command line argument) is a base url of InfluxDB API including protocol (http or https), ip or name and port (for example `http://127.0.0.1:8086`)
* `upsname` string parameter (`--upsname` command line argument) is UPS name reported in `ups_name` tag value to InfluxDB
* `username` string parameter (`--influxdb-user` command line argument) is InfluxDB user name. The parameter can be omitted if InfluxDB authorization is not required
* `password` string parameter (`--influxdb-password` command line argument) is InfluxDB password. The parameter can be omitted if InfluxDB authorization is not required
* `database` string parameter (`--influxdb-database` command line argument) is InfluxDB [database](https://docs.influxdata.com/influxdb/v1.8/concepts/key_concepts/#database) name. The parameter can be omitted. Default value is `telegraf` (matches default database name of [Telegraf](https://www.influxdata.com/time-series-platform/telegraf/) system metrics collector)
* `measurement` string parameter (`--influxdb-measurement` command line argument) is InfluxDB [measurement](https://docs.influxdata.com/influxdb/v1.8/concepts/key_concepts/#measurement) name. The parameter can be omitted. Default value is `ups`

### Setting config file path

Config file path can be passed as value of `--config` command line argument. 

## Logging

upsmon-pro-client uses log4rs crate (https://crates.io/crates/log4rs) for logging. Logging is configured via `/opt/upsmon-pro-client/log4rs.yaml` configuration file. Default config file writes text logs to `/var/log/` and stdout (which goes to service when it runs as systemd service).


