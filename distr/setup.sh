#!/usr/bin/env bash

INSTALL_DIR=/opt/upsmon-pro-client
SERVICE_NAME=upsmon-pro-client
SYSTEMD_UINITS_DIR=/lib/systemd/system

if systemctl is-active --quiet "${SERVICE_NAME}.service"; then
    echo -e "\e[2mStopping service $SERVICE_NAME\e[0m"
    systemctl stop "${SERVICE_NAME}.service"
fi    

echo -e "\e[2mCreating target directory ${INSTALL_DIR} if not exists\e[0m"
mkdir -p "$INSTALL_DIR"

echo -e "\e[2mCopying files to target directory\e[0m"
cp "./upsmon-pro-client" "${INSTALL_DIR}/"
cp "./LICENSE" "${INSTALL_DIR}/"
cp "./log4rs.yaml" "${INSTALL_DIR}/log4rs.yaml.default"
cp "./upsmon-pro-client.toml" "${INSTALL_DIR}/upsmon-pro-client.toml.default"

echo -e "\e[2mCopying service unit file to ${SYSTEMD_UINITS_DIR}\e[0m"
cp "./upsmon-pro-client.service" "${SYSTEMD_UINITS_DIR}/"

if [ ! -f "${INSTALL_DIR}/log4rs.yaml" ]; then
    echo -e "Installing default config log4rs.yaml"
    cp "./log4rs.yaml" "${INSTALL_DIR}/log4rs.yaml"
else 
    echo -e "\e[1mWARN: log4rs.yaml exists and will not be replaced. Copy defaults from log4rs.yaml.default by hand if necessary\e[0m"
fi

if [ ! -f "${INSTALL_DIR}/upsmon-pro-client.toml" ]; then
    echo -e "Installing default config upsmon-pro-client.toml"
    cp "./upsmon-pro-client.toml" "${INSTALL_DIR}/upsmon-pro-client.toml"
else 
    echo -e "\e[1mWARN: upsmon-pro-client.toml exists and will not be replaced. Copy defaults from upsmon-pro-client.toml.default by hand if necessary\e[0m"
fi

echo -e "\e[2mSetting file permissions\e[0m"
chmod 655 "$INSTALL_DIR"
chmod 644 $INSTALL_DIR/*
chmod +x "${INSTALL_DIR}/upsmon-pro-client"

echo -e "\e[2mReloading systemd unit files\e[0m"
systemctl daemon-reload
echo -e "\e[2mEnabling and starting service $SERVICE_NAME\e[0m"
systemctl enable --now "$SERVICE_NAME"
